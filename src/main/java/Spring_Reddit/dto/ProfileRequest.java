package Spring_Reddit.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@Data
@NoArgsConstructor
public class ProfileRequest {
    private long Id;
    private String firstname;
    private String lastname;
    private String DOB;
    private String address;
}
