package Spring_Reddit.security;

import Spring_Reddit.exception.PostNotFoundException;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Service;
import javax.annotation.PostConstruct;
import java.io.IOException;
import java.io.InputStream;
import java.security.*;
import java.security.cert.CertificateException;

@Service
public class JWTProvider {
    //provide token for user

    private KeyStore keyStore;
    @PostConstruct
    public void init()  {
        try {
            keyStore = keyStore.getInstance("JKS");
            InputStream resourceStream = getClass().getResourceAsStream("/springblog.jks");
            keyStore.load(resourceStream, "123456".toCharArray());

        } catch (CertificateException | IOException | NoSuchAlgorithmException | KeyStoreException e) {
            throw new PostNotFoundException("Exception while loading keystore");
        }


    }

    public final String generateToken(Authentication authentication) {
        org.springframework.security.core.userdetails.User principal = (User) authentication.getPrincipal();
        return Jwts.builder()
                .setSubject(principal.getUsername())
                .signWith(getPrivateKey())
                .compact();
    }

    private final PrivateKey getPrivateKey() {
        try {
            return (PrivateKey) keyStore.getKey("springblog", "123456".toCharArray());

        }catch (KeyStoreException  | NoSuchAlgorithmException | UnrecoverableKeyException e)
        {
            throw new PostNotFoundException("Exception while loading keystore");
        }

    }

    public final boolean validateToken(String jwt){
        Jwts.parser().setSigningKey(getPublicKey()).parseClaimsJws(jwt);
        return true;
    }

    private PublicKey getPublicKey() {
        try {
            return keyStore.getCertificate("springblog").getPublicKey();
        } catch (KeyStoreException e) {
            throw new PostNotFoundException("Exception while loading keystore");

        }
    }


    public final String getUsernameFromJwt(String token) {
        Claims claims = Jwts.parser()
                .setSigningKey(getPublicKey())
                .parseClaimsJws(token)
                .getBody();
        return claims.getSubject();
    }

}
