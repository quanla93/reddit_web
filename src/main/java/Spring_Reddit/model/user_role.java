package Spring_Reddit.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import  javax.persistence.GenerationType;



@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table
public class user_role {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long ID;
    @Column
    private long USER_ID;
    @Column
    private long ROLE_ID;
}
