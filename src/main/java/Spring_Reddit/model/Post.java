package Spring_Reddit.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.time.Instant;

@Entity
@Table
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Post {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long postId;
    @Column
    @NotBlank
    private String title;
    @Column
    @Lob //binary string
    @NotEmpty
    private String content;
    @Column
    private Instant createOn;
    @Column
    private Instant updateOn;
    @Column
    @NotBlank
    private String username;

}
