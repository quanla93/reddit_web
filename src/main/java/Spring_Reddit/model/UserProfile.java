package Spring_Reddit.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table
public class UserProfile {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long Id;
    @Column
    private String firstname;
    @Column
    private String lastname;
    @Column
    private String DOB;
    @Column
    private String address;
    @OneToOne
    @JoinColumn(name = "user_id")
    private User user;
}
