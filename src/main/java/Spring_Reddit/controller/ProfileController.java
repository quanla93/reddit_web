package Spring_Reddit.controller;

import Spring_Reddit.dto.ProfileRequest;
import Spring_Reddit.service.ProfileService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@AllArgsConstructor
@RequestMapping("/api/profile")
public class ProfileController {
    private final ProfileService profileService;
    @GetMapping("/{id}")
    public final ResponseEntity<ProfileRequest> getProfile(@PathVariable @RequestBody Long id) throws Throwable {
        return new ResponseEntity<ProfileRequest>(profileService.getProfilebyUser(id),HttpStatus.OK);
    }
    @PutMapping("/update")
    public final ResponseEntity updateProfile(@RequestBody ProfileRequest profileRequest) throws Throwable {
        profileService.updateProfile(profileRequest);
        return new ResponseEntity(HttpStatus.OK);
    }
}
