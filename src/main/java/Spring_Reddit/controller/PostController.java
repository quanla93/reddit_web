package Spring_Reddit.controller;

import Spring_Reddit.dto.PostRequest;
import Spring_Reddit.model.Post;
import Spring_Reddit.service.PostService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController

@RequestMapping("/api/posts")
public class PostController {

    private final PostService postService;

    public PostController(final PostService postService) {
        this.postService = postService;
    }

    @PostMapping("/add")
    public final ResponseEntity createPost(@RequestBody PostRequest postRequest) throws Throwable {
        postService.createPost(postRequest);
        return new ResponseEntity(HttpStatus.OK);
    }
    @GetMapping("/all")
    public final ResponseEntity<List<PostRequest>> listPost(){
        return new ResponseEntity<>(postService.getAllPost(), HttpStatus.OK);
    }

    @GetMapping("/get/{id}")
    public final ResponseEntity<PostRequest> getPostbyId(@PathVariable @RequestBody Long id){
        return new ResponseEntity<>(postService.getPostbyId(id), HttpStatus.OK);
    }
    @DeleteMapping("/deletepost/{id}")
    public final ResponseEntity deletePostbyUser(@PathVariable Long id)  {
        try {
            postService.deletePostByUser(id);
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
        return new ResponseEntity(HttpStatus.OK);
    }
    @PutMapping("/update")
     public final ResponseEntity<Post> updatePostbyUser(@RequestBody PostRequest postRequest)  {
        postService.updatePostById(postRequest);
        return new ResponseEntity<>( HttpStatus.OK);
    }

}
