package Spring_Reddit.controller;


import Spring_Reddit.dto.LoginRequest;
import Spring_Reddit.dto.RegisterRequest;
import Spring_Reddit.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@AllArgsConstructor
@RequestMapping("/api/auth")
public class UserController {

    private final UserService userService;


    @PostMapping("/signup")
    public final ResponseEntity signUp(@RequestBody RegisterRequest registerRequest){
            userService.signup(registerRequest);
            return new ResponseEntity<>(HttpStatus.OK);
    }


    @PostMapping("/login")
    public final String login(@RequestBody LoginRequest loginRequest) {
        return userService.login(loginRequest);
    }


}
