package Spring_Reddit.exception;

public class PostNotFoundException extends RuntimeException{
    public PostNotFoundException(String name){
        super(name);
    }

}
