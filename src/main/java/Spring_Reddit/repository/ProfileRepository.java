package Spring_Reddit.repository;

import Spring_Reddit.model.User;
import Spring_Reddit.model.UserProfile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface ProfileRepository extends JpaRepository<UserProfile, Long> {
    @Query("select u from UserProfile as u where u.user.userId = ?1")
    Optional<UserProfile> findByUser_id(long id);
}
