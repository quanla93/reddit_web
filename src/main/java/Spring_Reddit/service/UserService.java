package Spring_Reddit.service;


import Spring_Reddit.dto.LoginRequest;
import Spring_Reddit.dto.RegisterRequest;
import Spring_Reddit.model.User;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public interface UserService {
    void signup(RegisterRequest registerRequest);
    String login(LoginRequest loginRequest);

    <T> Optional getCurrentUser();
}
