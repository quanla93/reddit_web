package Spring_Reddit.service;

import Spring_Reddit.dto.PostRequest;
import Spring_Reddit.dto.ProfileRequest;
import Spring_Reddit.model.Post;
import Spring_Reddit.model.User;
import Spring_Reddit.model.UserProfile;
import Spring_Reddit.repository.ProfileRepository;
import Spring_Reddit.repository.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;


@Service
@AllArgsConstructor
public class ProfileServiceImp implements ProfileService{
    private final UserService userService;
    private final UserRepository userRepository;
    private final ProfileRepository profileRepository;

    private org.springframework.security.core.userdetails.User username() throws Throwable {
        return  (org.springframework.security.core.userdetails.User)
                userService.getCurrentUser().orElseThrow(() -> new IllegalArgumentException("User not Found"));
    }
    @Override
    public final ProfileRequest getProfilebyUser(long id) throws Throwable {
            User user = userRepository.findByUsername( username().getUsername()).orElseThrow(() -> new IllegalArgumentException("User not Found1"));
            UserProfile userProfile = profileRepository.findByUser_id(user.getUserId()).orElseThrow(() -> new IllegalArgumentException("User not Found2"));
        return mapFromPostToRequest(userProfile);
    }

    @Override
    public void updateProfile(ProfileRequest profileRequest) throws Throwable {
        User user = userRepository.findByUsername(username().getUsername()).orElseThrow(() -> new IllegalArgumentException("User not Found1"));

            UserProfile userProfile = profileRepository.findByUser_id(user.getUserId()).orElseThrow(() -> new IllegalArgumentException("User not Found2"));
            userProfile.setFirstname(profileRequest.getFirstname());
            userProfile.setLastname(profileRequest.getLastname());
            userProfile.setAddress(profileRequest.getAddress());
            userProfile.setDOB(profileRequest.getDOB());
            profileRepository.save(userProfile);

    }

    private final ProfileRequest mapFromPostToRequest(UserProfile userProfile) {
        ProfileRequest profileRequest = new ProfileRequest();
        profileRequest.setId(userProfile.getId());
        profileRequest.setFirstname(userProfile.getFirstname());
        profileRequest.setLastname(userProfile.getLastname());
        profileRequest.setDOB(userProfile.getDOB());
        profileRequest.setAddress(userProfile.getAddress());
        return profileRequest;
    }
}
