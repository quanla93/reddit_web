package Spring_Reddit.service;

import Spring_Reddit.dto.ProfileRequest;
import Spring_Reddit.model.UserProfile;


public interface ProfileService {
   ProfileRequest getProfilebyUser(long id) throws Throwable;
   void updateProfile(ProfileRequest profileRequest) throws Throwable;
}
