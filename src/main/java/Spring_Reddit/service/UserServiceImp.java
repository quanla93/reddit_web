package Spring_Reddit.service;

import Spring_Reddit.dto.LoginRequest;
import Spring_Reddit.dto.RegisterRequest;
import Spring_Reddit.exception.PostNotFoundException;
import Spring_Reddit.model.User;
import Spring_Reddit.model.UserProfile;
import Spring_Reddit.repository.ProfileRepository;
import Spring_Reddit.repository.UserRepository;
import Spring_Reddit.security.JWTProvider;
import lombok.AllArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@AllArgsConstructor
public class UserServiceImp implements UserService {

    private final UserRepository userRepository;

    private final PasswordEncoder passwordEncoder;

    private final AuthenticationManager authenticationManager;

    private final JWTProvider jwtProvider;

    private final ProfileRepository profileRepository;

    @Override
    public final void signup(RegisterRequest registerRequest) {
        User user = new User();
        user.setUsername(registerRequest.getUsername());
        user.setPassword(encodePassword(registerRequest.getPassword()));
        user.setEmail(registerRequest.getEmail());
        userRepository.save(user);

        User user1 = userRepository.findById(user.getUserId()).orElseThrow();
        UserProfile userProfile = new UserProfile();
        userProfile.setUser(user1);
        profileRepository.save(userProfile);

    }

    @Override
    public final String login(LoginRequest loginRequest) {
        Authentication authenticate = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(loginRequest.getUsername(),
                loginRequest.getPassword()));
        SecurityContextHolder.getContext().setAuthentication(authenticate);
        return jwtProvider.generateToken(authenticate);

    }

    @Override
    public Optional<org.springframework.security.core.userdetails.User> getCurrentUser() {
        org.springframework.security.core.userdetails.User principal = (org.springframework.security.core.userdetails.User)
                SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return Optional.of(principal);
    }


    private final String encodePassword(String password) {

        return passwordEncoder.encode(password);
    }
}
