package Spring_Reddit.service;

import Spring_Reddit.dto.PostRequest;
import Spring_Reddit.exception.PostNotFoundException;
import Spring_Reddit.model.Post;
import Spring_Reddit.repository.PostRepository;
import lombok.AllArgsConstructor;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class PostServiceImp implements PostService{

    private final UserService userService;
    private final PostRepository postRepository;

    @Override
    public final void createPost(PostRequest postRequest) throws Throwable {
        Post post = new Post();
        post.setTitle(postRequest.getTitle());
        post.setContent(postRequest.getContent());
        User username = (User) userService.getCurrentUser().orElseThrow(() -> new IllegalArgumentException("No user Logged"));

        post.setUsername(username.getUsername());
        post.setCreateOn(Instant.now());
        postRepository.save(post);
    }

    @Override
    public final List<PostRequest> getAllPost() {
        List<Post> post = postRepository.findAll();
        return post.stream().map(this::mapFromPostToRequest).collect(Collectors.toList());
    }

    @Override
    public PostRequest getPostbyId(Long id) {
        Post post = postRepository.findById(id).orElseThrow(() -> new PostNotFoundException("can't found the post by id " + id));
        return mapFromPostToRequest(post);
    }

    @Override
    public void deletePostByUser(Long id)  {
        User username = null;
        try {
            username = (User) userService.getCurrentUser().orElseThrow(() -> new IllegalArgumentException("No user Logged"));
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
        Post post = postRepository.findById(id).orElseThrow(() -> new PostNotFoundException("can't found the post by id " + id));
        if (post.getUsername().equals(username.getUsername())){
            postRepository.delete(post);
        }

    }

    @Override
    public final Post updatePostById(PostRequest postRequest) throws IllegalArgumentException {
        User username = null;
        try {
            username = (User) userService.getCurrentUser().orElseThrow(() -> new IllegalArgumentException("No user Logged"));
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
        Post post = postRepository.findById(postRequest.getId())
                .orElseThrow(() -> new PostNotFoundException("can't found the post by id "));
        if(username.getUsername().equals(post.getUsername())){
           post.setTitle(postRequest.getTitle());
           post.setContent(postRequest.getContent());
           post.setUpdateOn(Instant.now());
           post.setUsername(username.getUsername());
           postRepository.save(post);
        }
        return post;
    }

    private final PostRequest mapFromPostToRequest(Post post) {
        PostRequest postR = new PostRequest();
        postR.setId(post.getPostId());
        postR.setTitle(post.getTitle());
        postR.setContent(post.getContent());
        postR.setUsername(post.getUsername());
        return postR;
    }
    private final Post mapFromRequestToPost(PostRequest postRequest) throws Throwable {
        Post post = new Post();
        post.setPostId(postRequest.getId());
        post.setTitle(postRequest.getTitle());
        post.setContent(postRequest.getContent());
        post.setUsername(postRequest.getUsername());
        User username = (User) userService.getCurrentUser().orElseThrow(() -> new IllegalArgumentException("User not Found"));
        post.setCreateOn(Instant.now());
        post.setUpdateOn(Instant.now());
        post.setUsername(username.getUsername());
        return post;
    }
}
