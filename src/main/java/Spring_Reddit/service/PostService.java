package Spring_Reddit.service;

import Spring_Reddit.dto.PostRequest;
import Spring_Reddit.model.Post;

import java.util.List;

public interface PostService {
    void createPost(PostRequest postRequest) throws Throwable;
    List<PostRequest> getAllPost();
    PostRequest getPostbyId(Long id);
    void deletePostByUser(Long id) ;
    Post updatePostById(PostRequest postRequest) ;
}
